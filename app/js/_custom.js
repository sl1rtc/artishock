$(document).ready(function () {

    // SVG USE FOR IE
    svg4everybody();

    // Главный слайдер
    $(".mainSlider").slick({
        infinite: true,
        speed: 100,
        fade:true,
        arrows:true,
        dots:true,
        autoplay: true,
        autoplaySpeed: 3000,
        pauseOnFocus:false,
        pauseOnHover: false,
        swipe:true,
        adaptiveHeight: true,
        prevArrow:"<button type='button' class='slick-prev'><svg width='78px' height='78px'><use xlink:href='images/sprite.svg#project-arrow'></use></svg></button>",
        nextArrow:"<button type='button' class='slick-next'><svg width='78px' height='78px'><use xlink:href='images/sprite.svg#project-arrow'></use></svg></button>",
    }).on('beforeChange',function (event, slick, currentSlide, nextSlide){
        $(slick.$slides.get(currentSlide)).addClass("beforeslide");

        if(currentSlide>nextSlide || currentSlide==0)
        {
            $(slick.$slides.get(nextSlide)).addClass("prepared");
        }
        else
        {
            $(slick.$slides.get(nextSlide)).addClass("prepared");
        }
    }).on('afterChange',function (event, slick, currentSlide, nextSlide, addInfo){

        if(nextSlide>currentSlide || nextSlide==0)
        {
            $(slick.$slides.get(currentSlide)).addClass("animated-fromleft");
        }
        else
        {
            $(slick.$slides.get(currentSlide)).addClass("animated");
        }

        $(".prepared").removeClass("prepared");

        setTimeout(function(){
            $(".beforeslide").removeClass("beforeslide");
        },900)
    });

    // Слайдер 17 лет

    $(".mainYearsSlider").slick({
        infinite: false,
        speed: 600,
        arrows:true,
        dots:false,
        swipe:true,
        adaptiveHeight: true,
        initialSlide: 0,
        slidesToShow: 4,
        slidesToScroll: 4,
        prevArrow:"<button type='button' class='slick-prev'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        nextArrow:"<button type='button' class='slick-next'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        responsive: [
            {
                breakpoint: 1500,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }
        ]
    });

    // Слайдер спецпредложения

    $(".mainSpecial__list").slick({
        infinite: false,
        speed: 600,
        arrows:true,
        dots:false,
        swipe:true,
        adaptiveHeight: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        prevArrow:"<button type='button' class='slick-prev'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        nextArrow:"<button type='button' class='slick-next'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 760,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    $(".mainEquipment__video").hover(function (){
        $(".mainEquipment__content").addClass("slide");
        $('.mainEquipment__video video')[0].volume = 0.2;
        $('.mainEquipment__video video')[0].play();
    }, function (){
        $(".mainEquipment__content").removeClass("slide");
        $('.mainEquipment__video video')[0].pause();
    });

    $(".slabSlider").slick({
        infinite: false,
        speed: 600,
        arrows:true,
        dots:false,
        swipe:true,
        variableWidth:true,
        slidesToScroll: 1,
        prevArrow:"<button type='button' class='slick-prev'><svg width='24px' height='14px'><use xlink:href='images/sprite.svg#slab-arrow'></use></svg></button>",
        nextArrow:"<button type='button' class='slick-next'><svg width='24px' height='14px'><use xlink:href='images/sprite.svg#slab-arrow'></use></svg></button>",
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    arrows:false,
                }
            }
        ]
    });

    $(".mainClients__list").slick({
        infinite: false,
        speed: 600,
        arrows:true,
        dots:false,
        swipe:true,
        variableWidth:true,
        slidesToScroll: 1,
        prevArrow:"<button type='button' class='slick-prev'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        nextArrow:"<button type='button' class='slick-next'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
    });



    $(".mainNews__list").slick({
        infinite: false,
        speed: 600,
        arrows:true,
        dots:false,
        swipe:true,
        adaptiveHeight: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        prevArrow:"<button type='button' class='slick-prev'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        nextArrow:"<button type='button' class='slick-next'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 760,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });


    $(".mainShowroom__list").slick({
        infinite: false,
        speed: 600,
        arrows:true,
        dots:false,
        swipe:true,
        adaptiveHeight: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        prevArrow:"<button type='button' class='slick-prev'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        nextArrow:"<button type='button' class='slick-next'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    arrows:false,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows:false,
                }
            },
            {
                breakpoint: 760,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows:false,
                }
            }
        ]
    });


    //Инстаслайдер

    $(".instagramSlider__list").slick({
        infinite: true,
        speed: 600,
        arrows:true,
        dots:false,
        swipe:true,
        slidesToShow: 2,
        slidesToScroll: 2,
        prevArrow:"<button type='button' class='slick-prev'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        nextArrow:"<button type='button' class='slick-next'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows:true,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows:true,
                }
            },
            {
                breakpoint: 760,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows:false,
                }
            }
        ]
    });

    //Проекты

    $(".projectsSlider__list").slick({
        infinite: true,
        speed: 600,
        arrows:true,
        dots:false,
        swipe:true,
        adaptiveHeight: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        prevArrow:"<button type='button' class='slick-prev'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        nextArrow:"<button type='button' class='slick-next'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    arrows:true,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows:true,
                }
            },
            {
                breakpoint: 760,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows:false,
                }
            }
        ]
    });

    {
        //Проекты и их фильтры

        $(".mobileTags-projects").click(function (){
            $(".projectsTags").addClass('show');
            $("html,body").css("overflow","hidden");
            return false;
        });

        $(".projectsTags__list a").click(function (){
            $(this).toggleClass("active");
            return false;
        });


        $(".mobileSort-projects").click(function (){
            $(".projectsFilter__item--sort").addClass('show');
            $("html,body").css("overflow","hidden");
            return false;
        });

        $(".mobileFilters-projects").click(function (){
            $(".projectsFilter__filters").addClass('show');
            $("html,body").css("overflow","hidden");
            return false;
        });

        $(".projects__btns .close").click(function (){
            $(this).parent().removeClass("show");
            $("html,body").css("overflow","inherit");
            return false;
        });

        $(".projectsFilterPopup--sort .back").click(function (){
            $(this).parent().parent().parent().removeClass("show");
            $("html,body").css("overflow","inherit");
            return false;
        });

        $(".projectsFilterPopup__title").click(function (){
            $(this).parent().addClass('show');
            return false;
        });

        $(".projectsFilter__title .back").click(function (){
            $(this).parent().parent().removeClass("show");
            $("html,body").css("overflow","inherit");
            return false;
        });

        $(".projectsFilter__filters .projectsFilterPopup__title .back").click(function (){
            $(this).parent().parent().removeClass('show');
            $(".projectsFilter__filters").addClass('show');
            return false;
        });
    }


    {
        //Катеогрия товаров и фильтры

        $(".mobileTags").click(function (){
            $(".categoryTags").addClass('show');
            $("html,body").css("overflow","hidden");
            return false;
        });

        $(".categoryTags__list a").click(function (){
            $(this).toggleClass("active");
            return false;
        });

        $(".mobileSort").click(function (){
            $(".categoryFilter__item--sort").addClass('show');
            $("html,body").css("overflow","hidden");
            return false;
        });

        $(".mobileFilters").click(function (){
            $(".categoryFilter__filters").addClass('show');
            $("html,body").css("overflow","hidden");
            return false;
        });


        $(".category__btns .close").click(function (){
            $(this).parent().removeClass("show");
            $("html,body").css("overflow","inherit");
            return false;
        });

        $(".categoryTags .back").click(function (){
            $(this).parent().parent().removeClass("show");
            $("html,body").css("overflow","inherit");
            return false;
        });

        $(".categoryFilterPopup--sort .back").click(function (){
            $(this).parent().parent().parent().removeClass("show");
            $("html,body").css("overflow","inherit");
            return false;
        });

        $(".categoryFilterPopup__title").click(function (){
            $(this).parent().addClass('show');
            return false;
        });

        $(".categoryFilter__title .back").click(function (){
            $(this).parent().parent().removeClass("show");
            $("html,body").css("overflow","inherit");
            return false;
        });

        $(".categoryFilter__filters .categoryFilterPopup__title .back").click(function (){
            $(this).parent().parent().removeClass('show');
            $(".categoryFilter__filters").addClass('show');
            return false;
        });
    }


    $('.costSlider').slider({
        range: true,
        min: 0,
        max: 50000,
        values: [ 75, 30000 ],
        slide: function( event, ui ) {
            $(".categoryFilterPopup__cost [name='costFrom']").val(ui.values[ 0 ]);
            $(".categoryFilterPopup__cost [name='costTo']").val( ui.values[ 1 ]);
        }
    });

    var windowWidthSize = $(window).width();
    $(window).resize(function (){
        //При изменении размера окна скрываем элементы
        if(windowWidthSize != $(window).width())
        {
            $(".category").find(".show").removeClass("show");
            $(".header__catalogNav").removeClass("show");
            $(".header__catalog").removeClass("opened");
            $("html,body").css("overflow","inherit");
            windowWidthSize == $(window).width();
        }
    });

    $(".categoryFilter__opener").click(function (){
        $(".categoryFilterPopup").removeClass("show");
        $(this).parent().find(".categoryFilterPopup").addClass("show");
        $(".categoryFilter__opener").removeClass("active");
        $(this).addClass("active");
        return false; 
    });

    $(".projectsFilter__opener").click(function (){
        $(".projectsFilterPopup").removeClass("show");
        $(this).parent().find(".projectsFilterPopup").addClass("show");
        $(".projectsFilter__opener").removeClass("active");
        $(this).addClass("active");
        return false;
    });

    $("*").click(function (e){
        if($(e.target).closest(".categoryFilter__opener").length==0 && $(e.target).closest(".categoryFilterPopup").length==0)
        {
            $(".categoryFilter__opener").removeClass("active");
            $(".categoryFilterPopup").removeClass("show");
        }

        if($(e.target).closest(".projectsFilter__opener").length==0 && $(e.target).closest(".projectsFilterPopup").length==0)
        {
            $(".projectsFilter__opener").removeClass("active");
            $(".projectsFilterPopup").removeClass("show");
        }
    });

    autosize($("#autosize"));

    $("#autosize").keyup(function (){
        if($(this).val()!='')
        {
            $(this).parent().find(".searchForm__clear").addClass("show");
        }
        else
        {
            $(this).parent().find(".searchForm__clear").removeClass("show");
        }
    })

    $("#autosize").keyup();

    $(".header__searchBtn").click(function (){

        $(this).toggleClass("opened");
        $(".searchPopup").toggleClass("show");
        $(".header").toggleClass("searchToggle");
        $("html,body").scrollTop('0');

        if($(".header__searchBtn").hasClass("opened"))
        {
            $("html,body").css("overflow","hidden");
        }
        else
        {
            $("html,body").css("overflow","inherit");
        }

    });

    $(".searchPopup input").on("keyup change reset", function (){
        if($(this).val()!='')
        {
            $(".searchPopup .clear").show();
            $(".searchPopup__bottom").addClass("results");
        }
        else
        {
            $(".searchPopup .clear").hide();
            $(".searchPopup__bottom").removeClass("results");
        }
    });

    $(".searchPopup .clear").click(()=>{
        $(".searchPopup input").val("").change();
        return false;
    });



    $(".header__menuBtn").click(function (){
        $(this).toggleClass("opened");
        $(".headerMobileWrapper").toggleClass("show");

        if($(".header__menuBtn").hasClass("opened"))
        {
            $("html,body").css("overflow","hidden");
        }
        else
        {
            $("html,body").css("overflow","inherit");
        }
    });

    $(".header__catalog").click(function (){
        $(this).toggleClass("opened");
        $(".header__catalogNav").toggleClass("show");
        $(".header__catalogNav>ul>li:nth-child(2)>a").click();

        if($(".header__catalog").hasClass("opened"))
        {
            $("html,body").css("overflow","hidden");
        }
        else
        {
            $("html,body").css("overflow","inherit");
        }
    });

    $(".header__catalogNav>ul>li>a").click(function (){
        $(".header__catalogNav>ul>li>a").removeClass("opened");
        $(".header__catalogNav>ul>li>ul").removeClass("show");
        $(this).addClass("opened");
        $(this).parent().find(">ul").addClass("show");
        return false;
    });

    $(".headerMobileWrapper .back").click(function (){
        $(this).parent().parent().removeClass("show");
        return false;
    });

    $(".header__catalogNav .close").click(function (){
        $(".header__catalogNav").removeClass("show");
        $(".header__catalog").removeClass("opened");
        $("html,body").css("overflow","inherit");
        return false;
    });



    // Product page mobile slider
    {
        function productPageMobileSlider() {
            if ($(window).width() < 759) {
                $(".productPageImages__list").slick({
                    infinite: false,
                    speed: 600,
                    arrows:false,
                    dots:true,
                    swipe:true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                });
                $(".productPageImages__list").slick('setPosition');


                $(".checkoutAddress__list").slick({
                    infinite: false,
                    speed: 600,
                    arrows:false,
                    dots:false,
                    swipe:true,
                    adaptiveHeight: true,
                });
                $(".checkoutAddress__list").slick('setPosition');
            } else {
                if($(".productPageImages__list").hasClass("slick-slider"))
                    $('.productPageImages__list').slick("unslick");
                if($(".checkoutAddress__list").hasClass("slick-slider"))
                    $('.checkoutAddress__list').slick("unslick");
            }




        }
        productPageMobileSlider();
        var productPageMobileSliderTimer;
        $(window).resize(function() {
            clearTimeout(productPageMobileSliderTimer);
            productPageMobileSliderTimer = setTimeout(productPageMobileSlider, 50);
        });

    }


    function showMoreImagesTablet(){
        $(".productPageImages .hidden").hide();
        if($(window).width()>=760 && $(window).width()<1200){
            let i = 1;

            $(".productPageImages .hidden").each(function (){
                if(i<7)
                {
                    $(this).show();
                }
                i++;
            });
        }

    }

    showMoreImagesTablet();
    $(window).resize(function (){
        showMoreImagesTablet();
    });

    $(".articleSlider").slick({
        infinite: false,
        speed: 600,
        arrows:false,
        dots:false,
        swipe:true,
        variableWidth: true,
    });

    var ts;

    // $(".cartList__item").bind('touchstart', function (e){
    //     ts = e.originalEvent.touches[0].clientX;
    // });
    //
    // $(".cartList__item").bind('touchend', function(e) {
    //     var te = e.originalEvent.changedTouches[0].clientX;
    //     if(ts > te+50){
    //         $(this).css('transform','translateX(-75px)');
    //     }else{
    //         $(this).css('transform','translateX(0px)');
    //     }
    // });


    var switchState = 1;
    function switchSet(el,id){
        if(id==1)
        {
            switchState = 1;
        }
        else if(id==2)
        {
            switchState = 2;
        }
        else  if(id==3){
            if(switchState==1) switchState=2;
            else if(switchState==2) switchState=1;
        }

        el.find(".productPageSwitch__text, .checkoutSwitch__text").removeClass('active');
        el.find(".productPageSwitch__trigger, .checkoutSwitch__trigger").removeClass('active');

        if(switchState==1)
        {
            el.find(".productPageSwitch__text:first-child, .checkoutSwitch__text:first-child").addClass('active');
        }

        if(switchState==2)
        {
            el.find(".productPageSwitch__trigger, .checkoutSwitch__trigger").addClass('active');
            el.find(".productPageSwitch__text:last-child, .checkoutSwitch__text:last-child").addClass('active');
        }
    }

    $(".productPageSwitch .productPageSwitch__text:first-child, .checkoutSwitch .checkoutSwitch__text:first-child").click(function (){
        switchSet($(this).parent(),1);
    });

    $(".productPageSwitch .productPageSwitch__text:last-child, .checkoutSwitch .checkoutSwitch__text:last-child").click(function (){
        switchSet($(this).parent(),2);
    });

    $(".productPageSwitch__trigger, .checkoutSwitch__trigger").click(function (){
        switchSet($(this).parent(),3);
    });

    $(".checkoutBlock input, .popup input").each(function (){
        if($(this).val()!='')
        {
            $(this).parent().addClass("open");
        }
    });

    $(".checkoutBlock input, .popup input").keyup(function (){
        if($(this).val()!='')
        {
            $(this).parent().addClass("open");
        }
        else
        {
            $(this).parent().removeClass("open");
        }
    });

    function calcSliderContent(){
        document.querySelectorAll(".aboutSlider__item").forEach(function (item){
            // console.log(item.getElementsByClassName("aboutSlider__image")[0].getElementsByTagName("picture")[0].getElementsByTagName("img"));
            // console.log(item.getElementsByClassName("aboutSlider__image")[0].getElementsByTagName("picture")[0].getElementsByTagName("img")[0].offsetWidth);
            // let contentWidth = item.getElementsByClassName("aboutSlider__image")[0].getElementsByTagName("picture")[0].getElementsByTagName("img")[0].naturalWidth;
            let imgWidth = item.getElementsByClassName("aboutSlider__image")[0].getElementsByTagName("picture")[0].getElementsByTagName("img")[0].naturalWidth;
            let imgHeight = item.getElementsByClassName("aboutSlider__image")[0].getElementsByTagName("picture")[0].getElementsByTagName("img")[0].naturalHeight;
            let blockHeight = item.getElementsByClassName("aboutSlider__image")[0].clientHeight;
            console.log(blockHeight);
            let contentWidth = (blockHeight/imgHeight) * imgWidth;
            item.getElementsByClassName("aboutSlider__content")[0].style.width = contentWidth+'px';
        });
    }

    calcSliderContent();

    $(window).resize(function (){
        calcSliderContent();
    });

    $(".aboutSlider").slick({
        infinite: false,
        speed: 600,
        arrows:false,
        dots:false,
        swipe:true,
        variableWidth: true,
    });


    $(".dancingSlider__wrapper").slick({
        infinite: false,
        speed: 600,
        arrows:false,
        dots:false,
        swipe:true,
        adaptiveHeight: true,
        initialSlide: 0,
        slidesToShow: 4,
        slidesToScroll: 4,
        // prevArrow:"<button type='button' class='slick-prev'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        // nextArrow:"<button type='button' class='slick-next'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
        responsive: [
            {
                breakpoint: 1500,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    // $(".difficult__slider").slick({
    //     infinite: false,
    //     speed: 600,
    //     arrows:false,
    //     dots:false,
    //     swipe:true,
    //     adaptiveHeight: true,
    //     initialSlide: 0,
    //     slidesToShow: 4,
    //     slidesToScroll: 4,
    //     // prevArrow:"<button type='button' class='slick-prev'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
    //     // nextArrow:"<button type='button' class='slick-next'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
    //     responsive: [
    //         {
    //             breakpoint: 1500,
    //             settings: {
    //                 slidesToShow: 4,
    //                 slidesToScroll: 4,
    //             }
    //         },
    //         {
    //             breakpoint: 992,
    //             settings: {
    //                 slidesToShow: 2,
    //                 slidesToScroll: 2
    //             }
    //         },
    //         {
    //             breakpoint: 480,
    //             settings: {
    //                 slidesToShow: 1,
    //                 slidesToScroll: 1
    //             }
    //         }
    //     ]
    // });

    function tabletDesktopSlider() {
        if ($(window).width() > 759) {
            $(".difficult__slider").slick({
                infinite: false,
                speed: 600,
                arrows:false,
                dots:false,
                swipe:true,
                slidesToShow: 3,
                slidesToScroll: 3,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            }).on('beforeChange', function (event, { slideCount: count }, currentSlide, nextSlide){
                if(nextSlide==0)
                {
                    $(".difficult__text").removeClass("hide");
                }
                else
                {
                    $(".difficult__text").addClass("hide");
                }
            });

            $(".difficult__slider").slick('setPosition');
        } else {

            if($(".difficult__slider").hasClass("slick-slider")){
                $(".difficult__text").removeClass("hide");
                $('.difficult__slider').slick("unslick");
            }
        }
    }

    tabletDesktopSlider();
    var tabletDesktopSliderTimer;
    $(window).resize(function() {
        clearTimeout(tabletDesktopSliderTimer);
        tabletDesktopSliderTimer = setTimeout(tabletDesktopSlider, 50);
    });


    $(".projectSlider").slick({
        infinite: false,
        speed: 600,
        arrows:true,
        dots:false,
        swipe:true,
        variableWidth:true,
        slidesToScroll: 1,
        prevArrow:"<button type='button' class='slick-prev'><svg width='78px' height='78px'><use xlink:href='images/sprite.svg#project-arrow'></use></svg></button>",
        nextArrow:"<button type='button' class='slick-next'><svg width='78px' height='78px'><use xlink:href='images/sprite.svg#project-arrow'></use></svg></button>",
        responsive: [
            {
                breakpoint: 759,
                settings: {
                    arrows:false,
                    dots:true,
                }
            }
        ]
    });

    $(".projectProcess__slider").slick({
        infinite: false,
        speed: 600,
        arrows:false,
        dots:false,
        swipe:true,
        variableWidth:true,
        slidesToScroll: 1,
        prevArrow:"<button type='button' class='slick-prev'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#project-arrow'></use></svg></button>",
        nextArrow:"<button type='button' class='slick-next'><svg width='50px' height='50px'><use xlink:href='images/sprite.svg#project-arrow'></use></svg></button>",
        responsive: [
            {
                breakpoint: 760,
                settings: {
                    arrows:true,
                }
            }
        ]
    });


    $(".projectMaterialsPopup__close").click(function (){
        $(".projectSlider__btn").removeClass("active");
        $(".projectMaterialsPopup").removeClass("show");
    });

    $(".projectSlider__btn").click(function (){
        $(".projectMaterialsPopup").removeClass("show");
        $(".projectSlider__btn").removeClass("active");

        let popupId = $(this).data('material');
        $(".projectSlider__btn").removeClass("active");
        $(this).addClass("active");

        // Сперва скрываем предыдущие
        // $(".projectMaterialsPopup").removeClass("show");
        let thisProject =  $("#material-"+popupId);
        thisProject.addClass("show");

        let left = 0;


        let top = 0;

        if($(window).width() < 1200)
        {
            //слева справа аналогично дял мобилы и таблетки
            left = $(this).offset().left+7 - (thisProject.outerWidth()/2)
            if(left < 0)
            {
                left = 0;
            }
            if(left+thisProject.outerWidth() > $(window).width())
            {
                left = $(window).width() - thisProject.outerWidth();
            }
        }

        if($(window).width() < 760)
        {
            top = $(".projectSlider").offset().top;
        }

        if($(window).width() >= 760 && $(window).width() < 1200)
        {
            top = $(this).offset().top-5 - thisProject.outerHeight()
        }

        if($(window).width() >= 1200)
        {
            top = $(".projectSlider").offset().top - 10;

            left = $(this).offset().left+45

            if( left + thisProject.outerWidth() > $(window).width())
            {
                left = $(this).offset().left - thisProject.outerWidth() - 5;
            }

            // if(left < 0)
            // {
            //     left = 0;
            // }
            // if(left+$(".projectMaterialsPopup1").outerWidth() > $(window).width())
            // {
            //     left = $(window).width() - $(".projectMaterialsPopup1").outerWidth();
            // }
        }

        $("#material-"+popupId).css({
            "top": top,
            "left": left
        })

    });

    $("*").click(function (e){
        if($(e.target).closest(".projectMaterialsPopup").length==0 && $(e.target).closest(".projectSlider__btn").length==0)
        {
            $(".projectMaterialsPopup").removeClass("show");
            $(".projectSlider__btn").removeClass("active");
        }
    });

    $(".footer__checkbox [name='theme']").change(function (){
        if($(this).is(":checked"))
        {
            $("body").addClass('darkTheme');
            $.cookie('cookie-dark', 'true');
        }
        else
        {
            $("body").removeClass('darkTheme');
            $.cookie('cookie-dark', 'false');
        }
    });

    if($.cookie('cookie-dark') == 'true')
    {
        $("body").addClass('darkTheme');
        $(".footer__checkbox [name='theme']").attr("checked","checked");
    }

    $(".projectHide").click(function () {
        $(this).toggleClass("hidden");

        if($(this).hasClass("hidden"))
        {
            $(".projectSlider__btn").hide();
        }
        else
        {
            $(".projectSlider__btn").show();
        }

    });
















    // //cookie popup
    // if (!$.cookie('cookie-info')) {
    //     setTimeout(function(){
    //         $('.cookiePopup').addClass("show");
    //     }, 500);
    // }
    //
    // $(".cookieBtn").on("click",function () {
    //     $.cookie('cookie-info', true);
    //     $('.cookiePopup').removeClass("show");
    // });
    //
    // //start animation on load
    // $(".beforeanimate").removeClass("beforeanimate");
    //
    // //mobile detect
    // var mobileDevice = false;
    // (function(a){
    //     if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))
    //     {
    //         $("body").addClass('mobileDevice')
    //         mobileDevice=true;
    //     }
    // })(navigator.userAgent||navigator.vendor||window.opera);
    //
    // //header scroll fixed
    // var lastScrollTop = 0;
    // function resizeHeader() {
    //     $(".colorBg,.whiteBg").css({
    //         "padding-top": $(".header").outerHeight()
    //     })
    // }
    // resizeHeader();
    // $(window).resize(function (){
    //     resizeHeader();
    // });

    // Select styling
    // $("select").selectric();

    // Styling scroller on PC
    // $('.scrollbar-outer').scrollbar();

    //Tablet main product list slider
    //
    // function runMainProductsTabletSlider() {
    //     if ($(window).width() >= 768  && $(window).width() < 1152) {
    //         $(".mainProducts__list").slick({
    //             infinite: false,
    //             speed: 600,
    //             arrows:false,
    //             dots:false,
    //             swipe:true,
    //             slidesToShow: 2,
    //             slidesToScroll: 1,
    //         });
    //         $(".mainProducts__list").slick('setPosition');
    //     } else {
    //         if($(".cardList-main").hasClass("slick-slider"))
    //             $('.cardList-main').slick("unslick");
    //     }
    // }
    // runMainProductsTabletSlider();
    // var slidersTimer;
    // $(window).resize(function() {
    //     clearTimeout(slidersTimer);
    //     slidersTimer = setTimeout(runMainProductsTabletSlider, 50);
    // });


    //Open and closing menu
    //
    // if(mobileDevice===true || ($(window).outerWidth()<1152 && mobileDevice===false))
    // {
    //     $(".menuBtn").click(function (){
    //
    //         if(!$(".header__nav").hasClass("visible")){
    //             $(".header__nav").addClass("visible")
    //         }
    //     });
    // }
    // else if(mobileDevice===false) {
    //     console.log("не мобила");
    //     var timer = 0;
    //     $(".menuBtn").hover(function (){
    //         $(".header__nav").addClass("hover")
    //
    //     },function (){
    //         clearTimeout(timer);
    //         timer = setTimeout(function () {
    //             $(".header__nav").removeClass("hover")
    //         }, 100);
    //     });
    //
    //     $(".header__nav").hover(function (){
    //         clearTimeout(timer);
    //     }, function (){
    //         timer = setTimeout(function () {
    //             $(".header__nav").removeClass("hover")
    //         }, 100);
    //     });
    // }


    /*

    Open and close oil popup selector on XXL size page catalog

     */
    //
    // $(".radioSelector__title").click(function (){
    //     $(this).toggleClass("open");
    // });
    //
    // $("*").click(function (e){
    //     if($(e.target).closest(".radioSelector").length==0 && $(e.target).closest(".radioSelector__title").length==0)
    //     {
    //         $(".radioSelector__title").removeClass("open");
    //     }
    // });
    //
    /*

    Open and close filters

     */
    //
    // $(".catalogFilters__title").click(function (){
    //     $(this).toggleClass("open");
    // });

    /*

    Product tabs, Account tabs

     */
    //
    // $(".productTabs__links a").click(function (){
    //     let index = $(this).parent().index();
    //
    //     $(".productTabs__links li").removeClass("active");
    //     $(this).parent().addClass("active");
    //
    //
    //     $(".productTabs__content").removeClass("active")
    //     $(".productTabs__content:nth-child("+(index+1)+")").addClass("active");
    //
    //     return false
    // });
    //
    // $(".account__tabs a").click(function (){
    //     let index = $(this).parent().index();
    //
    //     $(".account__tabs a").removeClass("active");
    //     $(this).addClass("active");
    //
    //
    //     $(".account__content>*").removeClass("active")
    //     $(".account__content>*:nth-child("+(index+1)+")").addClass("active");
    //
    //     return false
    // });

    /*

    Accordion

    */
    //
    // $(".accordion__title").click(function (){
    //     $(this).parent().toggleClass("active");
    // });

    /*

    Открытие полей в полях вводу

    */
    //
    // $(".form label").on("click",function (){
    //    $(this).parent().addClass("open");
    //     $(this).parent().find("input").focus();
    // });
    //
    // $(".form input").on("focus",function (){
    //     $(this).parent().addClass("open");
    // });
    //
    // $(".form input").on("blur",function (){
    //     if($(this).val()==''){
    //         $(this).parent().removeClass("open");
    //     }
    // });
    //
    // $("input").each(function (){
    //     if($(this).val()!='') $(this).parent().addClass("open");
    // });



    // popup opening and closing
    //
    // $(".videoPopup").click(function (){
    //     $(".popup").css('z-index','150');
    //     $("html,body").css("overflow",'hidden');
    //     $("#videoPopup").show('200').css('z-index','160');
    //     let iframe = '<iframe src="' + "https://www.youtube.com/embed/" + $(this).attr("href").split("=")[1] + '" width="1245" height="730" frameborder="0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
    //     $(".videoPopup__iframe").html(iframe);
    //     return false;
    // });
    //

    $(".openPopup").click(function (e) {
        $(".popup").css('z-index','150');
        $("html,body").css("overflow",'hidden');
        // $("#" + $(this).data('popup')).fadeIn('200');

        if($(".popup").is(":visible"))
        {
            var lastPopup = $(".popup:visible");
            $("#" + $(this).data('popup')).fadeIn('200').css('z-index','16');
            setTimeout(function () {
                lastPopup.hide()
            },500)
        }
        else
        {
            $("#" + $(this).data('popup')).fadeIn('200');
        }

        if($(this).data('popup')=='packInfo' && $(window).width()>=992)
        {
            $("html,body").css("overflow",'visible');
        }
    });

    $(".popup__close").click(function (){
        $("html,body").css("overflow",'visible');
        $(".popup").fadeOut('200');
    });

    $("*").click(function (e) {
        if($(e.target).closest('.popup__container').length==0 && $(e.target).closest('.openPopup').length==0 && $(".popup").is(":visible"))
        {
            $("html,body").css("overflow",'inherit');
            $(".popup").fadeOut('200');
        }
    });

    $('.dancingSliderItem').fancybox({
        baseClass: 'dancingSliderPopup',
    });

    $('[data-fancybox="gallery"]').fancybox({
        baseClass: 'galleryWithThumbs',
        buttons: [
            //"zoom",
            //"share",
            //"slideShow",
            //"fullScreen",
            //"download",
            "thumbs",
            "close"
        ],
        thumbs: {
            autoStart: true,
            hideOnClose: true,
            axis: 'x'
        }
    })

    //Product page main slider
    //
    // $(".productPage__slider").slick({
    //     dots:true,
    //     prevArrow:"<button type='button' class='slick-prev'><svg width='15px' height='29px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
    //     nextArrow:"<button type='button' class='slick-next'><svg width='15px' height='29px'><use xlink:href='images/sprite.svg#slider-arrow'></use></svg></button>",
    // });
    //
    //
    // $(".header__cart").click(function (){
    //     $(".cartPopup").addClass("show");
    //     return false
    // });
    //
    // $(".cartPopup__close").click(function (){
    //     $(".cartPopup").removeClass("show");
    // });
    //
    // $(".cartPopup__code .apply").click(function (){
    //     $(".cartPopup__code").addClass("success");
    // });


    // init controller
    var controller = new ScrollMagic.Controller();

    if($(window).width()>1200)
    {

        function getRandomIntInclusive(min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min + 1)) + min; //Максимум и минимум включаются
        }

        $(".mainYearsSlider__animate").each(function (){
            var currentEl = this;

            var tweenExamples = TweenMax.to(currentEl, 1, { y: getRandomIntInclusive(-30,30) });

            var scene = new ScrollMagic.Scene({triggerElement: '.mainYearsSlider'})
                .setTween(tweenExamples)
                .addTo(controller);
        });

        $(".categoryItem").each(function (){
            var currentEl = this;

            var tweenExamples = TweenMax.to(currentEl, 1, { y: getRandomIntInclusive(0,20) });

            var scene = new ScrollMagic.Scene({triggerElement: currentEl})
                .setTween(tweenExamples)
                .addTo(controller);
        });


        var tweenExample1 = TweenMax.fromTo($(".mainDifficultyItem:nth-child(1),.mainDifficultyItem:nth-child(3)"), 1, { y: 70 }, { y: 0 });

        new ScrollMagic.Scene({triggerElement: ".mainDifficulty__list", duration:1000 })
            .setTween(tweenExample1)
            .addTo(controller);

        var tweenExample3 = TweenMax.fromTo($(".mainDifficultyOrder"), 1, { y: -70 }, { y: 0 });

        new ScrollMagic.Scene({triggerElement: ".mainDifficulty__list", duration:1000 })
            .setTween(tweenExample3)
            .addTo(controller);




        var tweenExample2 = TweenMax.fromTo($(".mainDifficultyItem:nth-child(2),.mainDifficultyItem:nth-child(4)"), 1, { y: 100 },{ y: 0 });

        new ScrollMagic.Scene({triggerElement: ".mainDifficulty__list", duration:1000 })
            .setTween(tweenExample2)
            .addTo(controller);


        $(".mainClients").each(function (){
            var currentEl = this;

            var tweenExamples = TweenMax.to(currentEl, 1, { "background-position": "50% -100px",backgroundPosition: "50% -100px" });

            var scene = new ScrollMagic.Scene({triggerElement: currentEl, duration: "100%"})
                .setTween(tweenExamples)
                .addTo(controller);
        });

        $(".subscribe").each(function (){
            var currentEl = this;

            var tweenExamples = TweenMax.to(currentEl, 1, { "background-position": "50% -100px",backgroundPosition: "50% -100px" });

            var scene = new ScrollMagic.Scene({triggerElement: currentEl, duration: "100%"})
                .setTween(tweenExamples)
                .addTo(controller);
        });
    }


    var projectShortTimer = 0;

    $(".projectShort").each(function (){
       let triggerPosition = 1;
       let thisProject = $(this);
        thisProject.find(".projectShort__triggers div").hover(function (){

            if(projectShortTimer!=0)
            {
                clearTimeout(projectShortTimer);
                thisProject.find(".projectShort__content .active").removeClass("rightSlided leftSlided");
                thisProject.find(".projectShort__content .prepared").removeClass("prepared");
            }

            let index = Number($(this).index()+1);
            let thisActive = thisProject.find(".projectShort__content .active");
            let newActive = thisProject.find(".projectShort__content img:nth-child("+index+")");
            if(newActive.hasClass("active")){
                return;
            }
            newActive.addClass("prepared");
            if(triggerPosition>index)
            {
                //двигаемся левее, анимация вправо, скрываем текущий активный слой
                thisActive.addClass("rightSlided");
            }
            else
            {
                //двигаемся правее, анимация влево, скрываем текущий активный слой
                thisActive.addClass("leftSlided");
            }
            projectShortTimer = setTimeout(function (){
                thisActive.removeClass("active");
                newActive.addClass("active");
                thisActive.removeClass("rightSlided leftSlided");
                newActive.removeClass("prepared");
                projectShortTimer=0;
            },400);
            triggerPosition = index;
       });
    });


    var projectItemTimer = 0;
    $(".projectItem").each(function (){
        let triggerPosition = 1;
        let thisProject = $(this);
        thisProject.find(".projectItem__triggers div").hover(function (){

            if(projectItemTimer!=0)
            {
                //Происходит преждевременное пролистывание, обнуляем слайдер
                clearTimeout(projectItemTimer);
                thisProject.find(".projectItem__content .active").removeClass("rightSlided leftSlided");
                thisProject.find(".projectItem__content .prepared").removeClass("prepared");
            }

            let index = Number($(this).index()+1);
            let thisActive = thisProject.find(".projectItem__content .active");
            let newActive = thisProject.find(".projectItem__content img:nth-child("+index+")");

            if(newActive.hasClass("active"))
            {
                return;
            }

            //console.log( thisActive.index() );

            newActive.addClass("prepared");

            if(triggerPosition>index)
            {
                //двигаемся левее, анимация вправо, скрываем текущий активный слой
                thisActive.addClass("rightSlided");
            }
            else
            {
                //двигаемся правее, анимация влево, скрываем текущий активный слой
                thisActive.addClass("leftSlided");
            }

            projectItemTimer = setTimeout(function (){
                thisActive.removeClass("active");
                newActive.addClass("active");
                thisActive.removeClass("rightSlided leftSlided");
                newActive.removeClass("prepared");
                projectItemTimer = 0;
            },400);

            triggerPosition = index;
        });

    });


});